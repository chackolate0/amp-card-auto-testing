import serial
from time import sleep
import struct
import numpy as np
import tempfile
import binascii
import serial.tools.list_ports


def _generate_cmd_prefix(rw, channel, modifier):
    """
    Generates the start of a command based on the desired channel

    rw:       int -> read/write modifier (read = 0, write = 1)
    channel:  int -> 1 for main output, 2 for auxiliary output
    modifier: str -> Modifier used to identify the command

    Returns: start of a command string for the desired channel
    """
    assert rw in range(2), "R/W value for prefix must be 0 (read) or 1 (write)"
    prefix_rw = "W" if rw == 1 else "R"
    assert channel in (1, 2), "Invalid channel, must be set to 1 (main) or 2 (auxilary)."
    prefix_channel = "M" if channel == 1 else "F"
    valid_modifiers = ("W", "F", "A", "O", "D", "P", "N")
    assert modifier in valid_modifiers, "Invalid cmd modifier (%s), must be in %s"%(modifier, valid_modifiers)
    return prefix_rw + prefix_channel + modifier


def generate_waveform_file(filepath, waveform_type, rise_time_points, fall_time_points, steady_time_points):
    """
    Generates an arbitrary waveform for the ldo signal in the form of a file-like object.

    filepath:           string -> Path and file for ldo waveform file
    waveform_type:      int -> Type of waveform (1 = ldo, 2 = mosfet)
    rise_time_points:   int -> Desired points (out of 8192) waveform is rising
    fall_time_points:   int -> Desired points (out of 8192) waveform is falling
    steady_time_points: int -> Desired points (out of 8192) waveform is steady (for ldo this is time spent low; for mosfet this is time spent high)

    Returns: Nothing
    """
    assert waveform_type in (1, 2), "Invalid waveform type, expected 1 (ldo) or 2 (mosfet), got %s"%(waveform_type)
    with open(filepath, "w") as f:
        if waveform_type == 1:
            _write_ldo_waveform(f, rise_time_points, fall_time_points, steady_time_points)
        else:
            _write_mosfet_waveform(f, rise_time_points, fall_time_points, steady_time_points)


def generate_temp_waveform(waveform_type, rise_time_points, fall_time_points, steady_time_points):
    """
    Generates an arbitrary waveform for the ldo signal in the form of a file-like object.

    waveform_type:      int -> Type of waveform (1 = ldo, 2 = mosfet)
    rise_time_points:   int -> Desired points (out of 8192) waveform is rising
    fall_time_points:   int -> Desired points (out of 8192) waveform is falling
    steady_time_points: int -> Desired points (out of 8192) waveform is steady (for ldo this is time spent low; for mosfet this is time spent high)

    Returns: TemporarayFile object
    """
    assert waveform_type in (1, 2), "Invalid waveform type, expected 1 (ldo) or 2 (mosfet), got %s"%(waveform_type)
    file = tempfile.TemporaryFile(mode="w+")
    if waveform_type == 1:
        _write_ldo_waveform(file, rise_time_points, fall_time_points, steady_time_points)
    else:
        _write_mosfet_waveform(file, rise_time_points, fall_time_points, steady_time_points)
    return file


def _write_ldo_waveform(file, rise_time_points, fall_time_points, steady_time_points):
    """
    Writes data for an arbitrary waveform for the ldo signal to a file-like object.

    file:               file-type -> wg signal file as a file-type object.
    rise_time_points:   int       -> Desired points (out of 8192) waveform is rising
    fall_time_points:   int       -> Desired points (out of 8192) waveform is falling
    steady_time_points: int       -> Desired points (out of 8192) waveform is steady (for ldo this is time spent low; for mosfet this is time spent high)

    Returns: nothing
    """
    # Start vals
    start_high = 1.0
    start_low = 0.0
    # Add falling points
    if fall_time_points > 0:
        slope_down = 1.0 / fall_time_points
        for _ in range(fall_time_points):
            start_high -= slope_down
            file.write((str("{0:.3f}".format(start_high)) + "\n"))
    # Add low steady points
    for _ in range(steady_time_points):
        file.write("0.0\n")
    # Add rising points
    if rise_time_points > 0:
        slope_up = 1.0 / rise_time_points
        for _ in range(rise_time_points):
            start_low += slope_up
            file.write((str("{0:.3f}".format(start_low)) + "\n"))
    # Add rest of signal (high)
    rest_of_signal = 8192 - (fall_time_points + steady_time_points + rise_time_points)
    for _ in range(rest_of_signal):
        file.write("1.0\n")


def _write_mosfet_waveform(file, rise_time_points, fall_time_points, steady_time_points):
    """
    Writes data for an arbitrary waveform for the mosfet signal to a file-like object.

    file:               file-type -> wg signal file as a file-type object.
    rise_time_points:   int       -> Desired points (out of 8192) waveform is rising
    fall_time_points:   int       -> Desired points (out of 8192) waveform is falling
    steady_time_points: int       -> Desired points (out of 8192) waveform is steady (for ldo this is time spent low; for mosfet this is time spent high)

    Returns: nothing
    """
    start_high = 1.0
    start_low = 0.0
    # Write rise points
    if rise_time_points > 0:
        slope_up = 1.0 / rise_time_points
        for _ in range(rise_time_points):
            start_low += slope_up
            file.write((str("{0:.3f}".format(start_low)) + "\n"))
    # Write steady (high) points
    for _ in range(steady_time_points):
        file.write("1.0\n")
    # Write fall points
    if fall_time_points > 0:
        slope_down = 1.0 / fall_time_points
        for _ in range(fall_time_points):
            start_high -= slope_down
            file.write((str("{0:.3f}".format(start_high)) + "\n"))
    # Write rest of signal (low)
    rest_of_signal = 8192 - (fall_time_points + steady_time_points + rise_time_points)
    for _ in range(rest_of_signal):
        file.write("0.0\n")


class FeelTechAWG(object):
    def __init__(self, port, **portsetup):
        self.debug = False
        self.device = serial.Serial(port, timeout=1, baudrate=115200)
        # Create dictionary for tracking waveform parameters. This is only set when the set_arbitrary_waveform() function is used
        # TODO: Is there any way to read the waveforms from the device? No way listed in any known documentation
        self.arb_waveform_params = {}
        for waveform_num in range(1, 65):
            self.arb_waveform_params[waveform_num] = {"rise_time_points": None, "fall_time_points": None, "steady_time_points": None}

    def close(self):
        self.device.close()

    def _handle_write_response(self, response, debug_str, channel, input_str, expected_response=b'\n'):
        """
        Returns whether the write command succeeded as a bool (if response == b'\n'). Also does a print if debug is enabled

        response:          bytes  -> response from a command
        debug str:         string -> used in debug print as an identifier
        channel:           int    -> channel used in the command (Set to None to not specify)
        input_str:         string -> input passed into command
        expected_response: bytes  -> expected response, set to None to skip check (NOT recommended)

        Returns: True if command was successful, False if command failed
        """
        cmd_succeeded = True
        if response == b'':
            raise TimeoutError
        if expected_response is not None and response != expected_response:
            cmd_succeeded = False
        if self.debug:
            debug_output = "[AWG DEBUG] COMMAND: %s, CHANNEL: %s, INPUT: %s, RESPONSE: %s, EXPECTED RESPONSE: %s - %s"%(debug_str, channel, input_str, str(response), str(expected_response), "SUCCESS" if cmd_succeeded else "FAILED")
            print(debug_output)
        return cmd_succeeded

    def send_cmd(self, cmd_str):
        self.device.flushInput()
        self.device.write((cmd_str + "\n").encode())
        self.device.flush()
        response = self.device.readline()
        sleep(0.5)
        return response

    def get_uplink_status(self):
        """
        Reads the uplink enable status (enabled/disabled)

        Returns: 255 (enabled), 0 (disabled)
        """
        response = int(self.send_cmd("RUL"))
        if self.debug: print("[AWG DEBUG] COMMAND: %s, RESPONSE: %s"%("get_uplink_status()", str(response)))
        return response

    def set_uplink_status(self, enable):
        """
        Sets the uplink enabel value (slave or master) from the AWG

        enable: int -> uplink enable value

        Returns: True if value was set, False if value failed to set
        """
        assert enable in (0, 1), "Invalid uplink status setting. Must be in 1 (enabled) or 0 (disabled)."
        cmd = "UUL" + str(enable)
        response = self.send_cmd(cmd)
        return self._handle_write_response(response, "set_uplink_status()", None, str(enable))

    def get_uplink_mode(self):
        """
        Reads the uplink mode value (slave or master) from the AWG

        Returns: 255 (slave), 0 (master)
        """
        response = int(self.send_cmd("RMS"))
        if self.debug: print("[AWG DEBUG] COMMAND: %s, RESPONSE: %s"%("get_uplink_mode()", str(response)))
        return response

    def set_uplink_mode(self, mode):
        """
        Sets the uplink mode value (slave or master) from the AWG

        mode: int -> uplink mode

        Returns: True if value was set, False if value failed to set
        """
        assert mode in (0, 1), "Invalid uplink mode setting. Must be in 1 (slave) or 0 (master)."
        cmd = "UMS" + str(mode)
        response = self.send_cmd(cmd)
        return self._handle_write_response(response, "set_uplink_mode()", None, str(mode))

    def get_on_off(self, channel):
        """
        Reads the current output status of a channel's (enabled or disabled)

        channel: int -> 1 for main output, 2 for auxiliary output

        Returns: 1 if enabled, 0 if disabled
        """
        cmd = _generate_cmd_prefix(0, channel, "N")
        response = int(self.send_cmd(cmd))
        if self.debug: print("[AWG DEBUG] COMMAND: %s, CHANNEL: %s, RESPONSE: %s"%("get_on_off()", channel, str(response)))
        output_status = 0 if response == 0 else 1
        return output_status

    def set_on_off(self, channel, output):
        """
        Turns the desired channel on or off

        channel: int -> 1 for main output, 2 for auxiliary output
        output:  int -> 0 to turn channel off, 1 to turn channel on

        Returns: True if value was set, False if value failed to set
        """
        cmd = _generate_cmd_prefix(1, channel, "N") + str(output)
        response = self.send_cmd(cmd)
        return self._handle_write_response(response, "set_on_off()", channel, str(output))

    def get_waveform(self, channel):
        """
        Reads the waveform type for the specified channel

        channel:   int        -> 1 for main output, 2 for auxiliary output

        Returns: waveform type as an integer. The main channel has an extra waveform at 5 (Adj-Pulse).
                Key vals (secondary waveform in parenthesis if different). For other values see FY6900 Serial communication protocol document:
                Sine: 0, Square: 1, Rectangle: 2, Arb1: 37 (36), Arb2: 38 (37)... ArbX (Up to 64): 36 + X (35 + X)
        """
        cmd = _generate_cmd_prefix(0, channel, "W")
        response = int(self.send_cmd(cmd))
        if self.debug: print("[AWG DEBUG] COMMAND: %s, CHANNEL: %s, RESPONSE: %s"%("get_waveform()", channel, str(response)))
        return response

    def set_waveform(self, channel, wave_type):
        """
        Sets the waveform type for the specified channel

        channel:   int -> 1 for main output, 2 for auxiliary output
        wave_type: int -> Pass in integer representation of desired waveform. The main channel has an extra waveform at 5 (Adj-Pulse).
                          Key inputs (secondary waveform in parenthesis if different):
                          Sine: 0, Square: 1, Rectangle: 2, Arb1: 37 (36), Arb2: 38 (37)... ArbX (Up to 64): 36 + X (35 + X)

        Returns: True if value was set, False if value failed to set
        """
        if channel == 2:
            assert wave_type in range(99), "Unrecognized wave integer: %d. Vaild values are 0-98 for channel 2."%(wave_type)
        else:
            assert wave_type in range(100), "Unrecognized wave integer: %d. Vaild values are 0-99 for channel 1."%(wave_type)
        waveform = str(wave_type)
        cmd = _generate_cmd_prefix(1, channel, "W") + waveform
        response = self.send_cmd(cmd)
        return self._handle_write_response(response, "set_waveform()", channel, str(wave_type))

    def get_frequency(self, channel):
        """
        Reads the specified channels frequency in Hz

        channel:   int -> 1 for main output, 2 for auxiliary output

        Returns: frequency in Hz
        """
        cmd = _generate_cmd_prefix(0, channel, "F")
        response = float(self.send_cmd(cmd))
        if self.debug: print("[AWG DEBUG] COMMAND: %s, CHANNEL: %s, RESPONSE: %s Hz"%("get_frequency()", channel, str(response)))
        return response

    def set_frequency(self, channel, frequency):
        """
        Sets the frequency for the desired channel. Value sent to AWG is in uHz

        channel:   int   -> 1 for main output, 2 for auxiliary output
        frequency: float -> Desired frequency in Hz

        Returns: True if value was set, False if value failed to set
        """
        assert 10**8 > frequency >= 10**-6, "AWG frequency must be greater than or equal to 1 uHz and less than 100 MHz"
        uHz_frequency = int(frequency * 1000000)
        cmd = _generate_cmd_prefix(1, channel, "F") + str(uHz_frequency)
        response = self.send_cmd(cmd)
        return self._handle_write_response(response, "set_frequency()", channel, str(frequency))

    def get_amplitude(self, channel):
        """
        Reads the amplitude of the specified channel. Value read from AWG is in mV. Returned value is converted to V.

        channel:   int   -> 1 for main output, 2 for auxiliary output

        Returns: amplitude in V
        """
        cmd = _generate_cmd_prefix(0, channel, "A")
        response = int(self.send_cmd(cmd)) # Response returned in mV * 10? Documentation says mV, but experimentation shows this is incorrect
        if self.debug: print("[AWG DEBUG] COMMAND: %s, CHANNEL: %s, RESPONSE: %s"%("get_amplitude()", channel, str(response)))
        return response / 10000

    def set_amplitude(self, channel, amplitude):
        """
        Sets the peak-to-peak amplitude for the desired channel in V

        channel:   int   -> 1 for main output, 2 for auxiliary output
        amplitude: float -> Amplitude of the waveform frequency in Hz
                            Frequency ≤ 5MHz:           1mVpp~24Vpp;
                            5MHz < Frequency ≤ 10MHz:   1mVpp~20Vpp;
                            10MHz < Frequency ≤ 20MHz:  1mVpp~10Vpp;
                            Frequency > 20MHz:          1mVpp~5Vpp;

        Returns: True if value was set, False if value failed to set
        """
        # Read current freq
        freq = self.get_frequency(channel)
        assert amplitude >= 10**-3, "Amplitude must be at least 1 mV"
        # Check amplitude based on freq
        if freq <= 5*10**6:
            assert amplitude <= 24, "Amplitude max is 24 V for the current frequency (given: %s)"%(amplitude)
        elif 5*10**6 < freq <= 10*10**6:
            assert amplitude <= 20, "Amplitude max is 20 V for the current frequency (given: %s)"%(amplitude)
        elif 10*10**6 < freq <= 20*10**6:
            assert amplitude <= 10, "Amplitude max is 10 V for the current frequency (given: %s)"%(amplitude)
        else:
            assert amplitude <= 5, "Amplitude max is 5 V for the current frequency (given: %s)"%(amplitude)
        # Send cmd
        cmd = _generate_cmd_prefix(1, channel, "A") + str(amplitude)
        response = self.send_cmd(cmd)
        return self._handle_write_response(response, "set_amplitude()", channel, str(amplitude))

    def get_offset(self, channel):
        """
        Reads the offset value for the desired channel. Value returned is mV value + 10000 (ie 6000 = -4000 mV, 12000 = 2000 mV)

        channel: int -> 1 for main output, 2 for auxiliary output

        Returns: offset in V
        """
        cmd = _generate_cmd_prefix(0, channel, "O")
        response = int(self.send_cmd(cmd))
        if self.debug:
            print("[AWG DEBUG] COMMAND: %s, CHANNEL: %s, RESPONSE: %s"%("get_offset()", channel, response))
        # Find two's compliment of response
        # Documentation lists a different method for finding offset,
        # but it seems like its a twos compliment (32 bits)... idk
        if (response & 0x80000000) != 0:
            response = response - 0x100000000
        return response / 1000

    def set_offset(self, channel, offset):
        """
        Sets the votage offset for the desired channel

        channel: int   -> 1 for main output, 2 for auxiliary output
        offset:  float -> Desired offset in V

        Returns: True if value was set, False if value failed to set
        """
        # Read current freq
        freq = self.get_frequency(channel)
        # Check boundaries
        if offset != 0:
            assert abs(offset) >= 0.001, "Minimum non-zero offset value must be at least 1 mV"
        if freq <= 20*10**6:
            assert abs(offset) <= 12, "Absolute value of offset cannot exceed 12 V for current frequency"
        else:
            assert abs(offset) <= 2.5, "Absolute value of offset cannot exceed 2.5 V for current frequency"
        # Set value
        cmd = _generate_cmd_prefix(1, channel, "O") + str(offset)
        response = self.send_cmd(cmd)
        return self._handle_write_response(response, "set_offset()", channel, str(offset))

    def get_duty_cycle(self, channel):
        """
        Reads the duty cycle for the desired channel. Original value is between 0 and 100000. Duty cycle is only applicable for Rectangle and CMOS waveforms.

        channel: int -> 1 for main output, 2 for auxiliary output

        Returns: Duty cycle as a percent value between 0.0 and 100.0
        """
        cmd = _generate_cmd_prefix(0, channel, "D")
        response = int(self.send_cmd(cmd))
        if self.debug: print("[AWG DEBUG] COMMAND: %s, CHANNEL: %s, RESPONSE: %s"%("get_duty_cycle()", channel, str(response)))
        return response / 1000

    def set_duty_cycle(self, channel, duty_cycle):
        """
        Sets the duty cycle for the desired channel as a percentage. Duty cycle is only changeable for Rectangle and CMOS waveforms.
        Setting the duty cycle on any other waveforms will update the AWG's value but will not have any affect on the current waveforms.

        channel:    int   -> 1 for main output, 2 for auxiliary output
        duty_cycle: float -> Desired duty cycle percentage as a float value out of 100.0 (ex: 20.2 = 20.2%)

        Returns: True if value was set, False if value failed to set
        """
        duty_cycle = round(duty_cycle, 1)
        assert 0 < duty_cycle < 100, "Duty cycle value must be greater than 0.0 and less than 100.0"
        cmd = _generate_cmd_prefix(1, channel, "D") + str(float(duty_cycle))
        response = self.send_cmd(cmd)
        return self._handle_write_response(response, "set_duty_cycle()", channel, str(duty_cycle))

    def get_phase(self, channel):
        """
        Reads phase value for desired channel. Read value is between 0 and 3600.

        channel: int -> 1 for main output, 2 for auxiliary output

        Returns: Duty cycle as a percent value between 0.0 and 359.9
        """
        cmd = _generate_cmd_prefix(0, channel, "P")
        response = int(self.send_cmd(cmd))
        if self.debug: print("[AWG DEBUG] COMMAND: %s, CHANNEL: %s, RESPONSE: %s"%("get_phase()", channel, str(response)))
        return response / 1000 # Documentation says this is returned in tenths of degrees, but the device reads in thousandths

    def set_phase(self, channel, phase):
        """
        Sets the duty cycle phase for the desired channel

        channel: int   -> 1 for main output, 2 for auxiliary output
        phase:   float -> degrees of phase as an int (min 0.0, max 359.9), resolution is 0.1
                          *Value is rounded to nearest tenths place*

        Returns: True if value was set, False if value failed to set
        """
        phase = round(phase, 1)
        assert 0 <= phase <= 359.9, "Phase value must be between 0 and 360"
        cmd = _generate_cmd_prefix(1, channel, "P") + str(float(phase))
        response = self.send_cmd(cmd)
        return self._handle_write_response(response, "set_phase()", channel, str(phase))

    def get_arbitrary_waveform_times(self, channel, waveform_number):
        """
        Reads the current arbitrary waveform counts and converts them to time based on the desired channel's frequency

        channel:         int   -> 1 for main output (used for low steady time), 2 for auxiliary output (used for high steady time)
        waveform_number: int   -> number for arbitrary waveform channel (1-64)

        Returns: dictionary of times in ms. Unknown values (if waveform counts were not set by this awg object) are returned as None
        """
        assert channel in (1, 2), "Invalid channel, must be set to 1 (main) or 2 (auxilary)."
        assert waveform_number in range(1, 65), "Unknown arbitrary waveform number. Expected 1-64, given %d."%(waveform_number)
        period_ms = 1000 / self.get_frequency(channel)
        time_dict = {"rise_time": None, "fall_time": None, "steady_time": None, "delay_time": None}
        for key in self.arb_waveform_params[waveform_number]:
            if self.arb_waveform_params[waveform_number][key] is not None:
                output_key = key.replace("_points", "")
                time_dict[output_key] = self.arb_waveform_params[waveform_number][key] * period_ms / 8192
        return time_dict

    def time_to_points(self, channel, rise_time, fall_time, steady_time):
        """
        Converts rise/fall/steady time to number of points based on channel's current frequency

        channel:         int   -> 1 for main output (used for low steady time), 2 for auxiliary output (used for high steady time)
        rise_time:       float -> desired rise time in ms
        fall_time:       float -> desired fall time in ms
        steady_time:     float -> desired steady time in ms

        Returns: rise_time_points, fall_time_points, steady_time_points
        """
        # Check values compared to freq
        period_ms = 1000 / self.get_frequency(channel)
        assert (rise_time + fall_time + steady_time) < period_ms, "The given rise time, fall time, and steady time are too large for the current frequency."
        # Convert time to points
        rise_time_points = round(8192 * rise_time / period_ms)
        fall_time_points = round(8192 * fall_time / period_ms)
        steady_time_points = round(8192 * steady_time / period_ms)
        return rise_time_points, fall_time_points, steady_time_points

    def set_arbitrary_waveform(self, channel, waveform_number, rise_time_points, fall_time_points, steady_time_points):
        """
        Converts rise/fall/steady time to counts based on current frequency, generates temporary waveform file, and sends the arbitrary waveform.

        channel:            int -> 1 for main output (used for low steady time), 2 for auxiliary output (used for high steady time)
        waveform_number:    int -> number for arbitrary waveform channel (1-64)
        rise_time_points:   int -> Desired points (out of 8192) waveform is rising
        fall_time_points:   int -> Desired points (out of 8192) waveform is falling
        steady_time_points: int -> Desired points (out of 8192) waveform is steady (for ldo this is time spent low; for mosfet this is time spent high)

        Returns: waveform return value
        """
        # Generate waveform and send to device
        file = generate_temp_waveform(channel, rise_time_points, fall_time_points, steady_time_points)
        send_output = self.send_arbitrary_waveform(file, waveform_number)
        assert send_output == 0, "Failed to send arbitrary waveform with error code %d."%(send_output)
        # Update waveform param list
        self.arb_waveform_params[waveform_number].update({"rise_time_points": rise_time_points, "fall_time_points": fall_time_points,
                                                        "steady_time_points": steady_time_points})
        # Set waveform type
        mod = 35 if channel == 1 else 34 # Arb1 = 36 for channel 1, 35 for channel 2
        return self.set_waveform(channel, mod + waveform_number)

    def send_arbitrary_waveform(self, signal_file, waveform_number):
        """
        Sends an aribitrary waveform to the device

        signal_file:     str/file object -> link to file used for arbitrary waveform, or file object with waveform
        waveform_number: int             -> number for arbitrary waveform channel (1-64)

        Returns: error code as int (0 = success, 1 = command failed at DDS_WAVE command, 2 = command failed at sending signal_file lines)
        """
        # TODO: Make documentation description better
        assert waveform_number in range(1, 65), "Unknown arbitrary waveform number. Expected 1-64, given %d."%(waveform_number)
        if isinstance(signal_file, str):
            signal_file = open(signal_file, "r")
        # Count lines (important to check since messing this causes awg to get stuck in a waiting state)
        signal_file.seek(0)
        count = 0
        for _line in signal_file: count += 1
        assert count == 8192, "Specified signal file has incorrect number of lines. Expected 8192, got %d."%(count)
        signal_file.seek(0)
        # Send DDS wave command
        cmd = "DDS_WAVE"
        cmd += str(waveform_number)
        self.device.flushInput()
        self.device.write((cmd + "\n").encode())
        self.device.flush()
        sleep(0.4)
        # If success, send file data
        if self.device.readline() == b'W\n':
            self.device.flushInput()
            for aline in signal_file:
                values = int(float(aline) * 16383)
                value2send = struct.unpack('<H',struct.pack('>H',values))[0]
                values2send = "0x{:04x}".format(value2send)
                self.device.write(bytes.fromhex(values2send[2:]))
            self.device.write(("\n").encode())
            sleep(2.0)
            response = self.device.read(5)
            success = self._handle_write_response(response, "send_arbitrary_waveform()", None, "signal_file=%s, waveform_number=%s"%(signal_file, str(waveform_number)), b'HN')
            return 0 if success else 2
        # Otherwise raise error
        else:
            if self.debug:
                print("Error: Sending of arbitrary waveform was not successful")
            return 1

def main():
    # Find the AWG
    ports = list(serial.tools.list_ports.comports())
    awg = None
    for p in ports:
        if "USB Serial" in p.description:
            print("Found the FeelTech AWG")
            awg = FeelTechAWG(p[0])
    assert awg is not None, "No AWG device found"

    # Turn both channels off
    awg.set_on_off(1, 0)
    awg.set_on_off(2, 0)

    # Set camera and NUCPC sync signal
    awg.set_waveform(1, 1)
    awg.set_frequency(1, 100)
    awg.set_amplitude(1, 3.5)
    awg.set_offset(1, 1.75)
    awg.set_phase(1, 0)

    # Set LDO control signal
    try:
        awg.send_arbitrary_waveform("0.1ms_rise-fall.txt", 2)       #This file name will depend on what signal is being sent
    except FileNotFoundError:
        print("Failed to send arbitrary waveform, file not found")
    awg.set_waveform(2, 38)
    awg.set_frequency(2, 100)
    awg.set_amplitude(2, 16)
    awg.set_offset(2, 0)
    awg.set_phase(2, 0)

    # Turn both channels back on
    awg.set_on_off(1, 1)
    awg.set_on_off(2, 1)
    awg.close()


if __name__ == "__main__":
    main()
