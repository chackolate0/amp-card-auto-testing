from ds1054z import *
import time
import numpy as np
import json

if __name__ == "__main__":
    scope = DS1054Z("10.2.4.92")
    print("Connected to: ", scope.idn)

    scope.display_channel("CHAN1", True)
    scope.set_probe_ratio("CHAN1", 10)
    scope.set_channel_scale("CHAN1", 1, False)

    scope.display_channel("CHAN2", True)
    scope.set_probe_ratio("CHAN2", 10)
    scope.set_channel_scale("CHAN2", 1, False)

    scope.display_channel("CHAN3", True)
    scope.set_probe_ratio("CHAN3", 10)
    scope.set_channel_scale("CHAN3", 1, False)

    scope.display_channel("CHAN4", True)
    scope.set_probe_ratio("CHAN4", 10)
    scope.set_channel_scale("CHAN4", 1, False)

    # print("Currently displayed channels: ", scope.displayed_channels)
    print("CHAN1 Ratio: ", scope.get_probe_ratio("CHAN1"))
    print("CHAN2 Ratio: ", scope.get_probe_ratio("CHAN2"))
    print("CHAN3 Ratio: ", scope.get_probe_ratio("CHAN3"))
    print("CHAN4 Ratio: ", scope.get_probe_ratio("CHAN4"))

    Chan1 = scope.get_waveform_samples("CHAN1", "NORMal")
    Chan2 = scope.get_waveform_samples("CHAN2", "NORMal")
    Chan3 = scope.get_waveform_samples("CHAN3", "NORMal")
    Chan4 = scope.get_waveform_samples("CHAN4", "NORMal")

    # for i in Chan1

    print(Chan1)
