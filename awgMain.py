import feeltech_awg
import serial
from time import sleep
import struct
import numpy as np
import tempfile
import binascii
import serial.tools.list_ports


def main():
    # Find the AWG
    print("Finding ports:")
    ports = list(serial.tools.list_ports.comports())
    for port, desc, hwid in sorted(ports):
        print("{}: {} [{}]".format(port, desc, hwid))
    awg = None
    for p in ports:
        print("USB Serial: {}".format(p.description))
        if "USB-SERIAL" in p.description:
            print("Found the FeelTech AWG")
            awg = feeltech_awg.FeelTechAWG(p[0])
    assert awg is not None, "No AWG device found"

    # Turn both channels off
    awg.set_on_off(1, 0)
    awg.set_on_off(2, 0)

    # Set camera and NUCPC sync signal
    awg.set_waveform(1, 1)
    awg.set_frequency(1, 1000000)
    awg.set_amplitude(1, 0.363)
    awg.set_offset(1, 0.318)
    awg.set_phase(1, 0)

    awg.set_waveform(2, 1)
    awg.set_frequency(2, 1000000)
    awg.set_amplitude(2, 0.363)
    awg.set_offset(2, 0.318)
    awg.set_phase(2, 0)

    awg.set_on_off(1, 1)
    awg.set_on_off(2, 1)


if __name__ == "__main__":
    main()
